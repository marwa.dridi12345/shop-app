package edu.esprit.shop;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import edu.esprit.shop.persistence.Customer;
import edu.esprit.shop.services.UserServiceLocal;

@ManagedBean
public class InscriptionBean {
	private String firstname;
	private String lastname;
	private String email;
	private String login;
	private String password;
	private Date birthdate;
	private String phone;

	@EJB
	UserServiceLocal service;
	Customer customer;

	@PostConstruct
	public void init() {
		customer = new Customer();
	}
	
	public String doSubscribtion(){
		customer.setFirstname(firstname);
		customer.setLastname(lastname);
		customer.setEmail(email);
		customer.setBirthDate(new Date());
		customer.setLogin(login);
		customer.setPassword(password);
		customer.setPhoneNumber(phone);
		service.saveCustomer(customer);
		return null;
	}

	public InscriptionBean() {
		super();
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}

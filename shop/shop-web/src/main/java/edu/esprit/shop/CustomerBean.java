package edu.esprit.shop;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import edu.esprit.shop.persistence.Product;
import edu.esprit.shop.services.CatalogServiceLocal;
import edu.esprit.shop.services.TransactionServiceLocal;

@ManagedBean
@ViewScoped
public class CustomerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3252651134553050074L;
	private Date date;
	private int quantity;
	private boolean displayForm;
	private boolean displayDetails;
	private List<Product> products;
	private Product product;

	@EJB
	CatalogServiceLocal catalogServiceLocal;
	@EJB
	TransactionServiceLocal transactionServiceLocal;

	@PostConstruct
	public void init() {
		setProduct(new Product());
		setProducts(catalogServiceLocal.findAllProducts());

	}

	@ManagedProperty("#{loginBean}")
	LoginBean lBean;

	public void saveTr() {

		boolean tr = false;

		System.out.println(lBean.getUser().getLogin());
		// date=new Date();
		if (quantity == 0) {
			FacesMessage message = new FacesMessage("specify the quantity");
			FacesContext.getCurrentInstance().addMessage("formadd:tsave",
					message);
		} else {
			tr = transactionServiceLocal.createTransaction(lBean.getUser()
					.getId(), product.getId(), date, quantity);
			if (tr) {
				FacesMessage message = new FacesMessage(
						"successful__transaction");
				FacesContext.getCurrentInstance().addMessage("formadd:tsave",
						message);

				products = catalogServiceLocal.findAllProducts();
				displayDetails = true;
			} else {
				FacesMessage message = new FacesMessage("failed__transaction");
				FacesContext.getCurrentInstance().addMessage("formadd:tsave",
						message);

			}

		}

	}

	public void onRowSelect(SelectEvent event) {
		setDisplayForm(true);
		System.out.println(product.getName());
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isDisplayForm() {
		return displayForm;
	}

	public void setDisplayForm(boolean displayForm) {
		this.displayForm = displayForm;
	}

	public boolean isDisplayDetails() {
		return displayDetails;
	}

	public void setDisplayDetails(boolean displayDetails) {
		this.displayDetails = displayDetails;
	}

	public LoginBean getlBean() {
		return lBean;
	}

	public void setlBean(LoginBean lBean) {
		this.lBean = lBean;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}

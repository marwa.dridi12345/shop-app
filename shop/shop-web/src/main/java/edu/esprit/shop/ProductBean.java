package edu.esprit.shop;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import edu.esprit.shop.persistence.Category;
import edu.esprit.shop.persistence.Product;
import edu.esprit.shop.services.CatalogServiceLocal;

@ManagedBean
@ViewScoped
public class ProductBean implements Serializable {

	
	
	private static final long serialVersionUID = 1406477953799338038L;

	private Product product;

	@EJB
	private CatalogServiceLocal catalogServiceLocal;
	private List<Category> categories;
	private List<Product> products;
	private boolean display;

	@PostConstruct
	public void init() {
		setProduct(new Product());
		categories = catalogServiceLocal.findAllCategories();
		products = catalogServiceLocal.findAllProducts();
		setDisplay(false);

	}

	public void onRowSelect(SelectEvent event) {
		setDisplay(true);
	}

	public void doSave() {

		catalogServiceLocal.saveProduct(product);
		products = catalogServiceLocal.findAllProducts();
		display = false;

	}

	public void doCancel() {
		product = new Product();
		display = false;
	}

	public void doDelete() {

		catalogServiceLocal.removeProduct(product);
		products = catalogServiceLocal.findAllProducts();
		product = new Product();
		display = false;

	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public void doNew() {
		setDisplay(true);
		product = new Product();

	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

}

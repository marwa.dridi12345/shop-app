package edu.esprit.shop;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.esprit.shop.persistence.Category;
import edu.esprit.shop.services.CatalogServiceLocal;

@ManagedBean
@ViewScoped
public class CategoryBean {

	

	@EJB
	CatalogServiceLocal catalogServiceLocal;

	private Category category;

	private List<Category> categories;

	private boolean displayForm = false;

	@PostConstruct
	public void init() {

		category = new Category();
		setCategories(catalogServiceLocal.findAllCategories());
	}



	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String doSave() {

		String navigateTo = null;
		
		catalogServiceLocal.saveCategory(category);
		category = new Category();
		setCategories(catalogServiceLocal.findAllCategories());
		displayForm = false;

		return navigateTo;
	}
	/*
	 * do cancel
	 */
	public String doCancel() {

		String navigateTo = null;
		category = new Category();
		
		displayForm=false;
		

		return navigateTo;
	}
	/*
	 * do delete 
	 */
	public String doDelete() {
		
		catalogServiceLocal.removeCategory(category);
		category = new Category();
		setCategories(catalogServiceLocal.findAllCategories());
		String navigateTo = null;
		displayForm=false;
		

		return navigateTo;
	}

	public String doNew() {
		String navigateTo = null;
		displayForm = true;

		return navigateTo;

	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public boolean isDisplayForm() {
		return displayForm;
	}

	public void setDisplayForm(boolean displayForm) {
		this.displayForm = displayForm;
	}

}

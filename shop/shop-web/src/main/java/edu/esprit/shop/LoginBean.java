package edu.esprit.shop;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import edu.esprit.shop.persistence.Admin;
import edu.esprit.shop.persistence.Customer;
import edu.esprit.shop.persistence.User;
import edu.esprit.shop.services.UserServiceLocal;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5343938244543300139L;

	private String login;

	private String password;
	private User user;
	private boolean loggedIn;
	private boolean displayLogin;
	@EJB
	private UserServiceLocal userServiceLocal;

	public LoginBean() {

	}

	@PostConstruct
	public void initModel() {
		user = new User();
		loggedIn = false;
		setDisplayLogin(false);
	}

	public String doLogin() {

		
		user = userServiceLocal.authenticate(login, password);
		if (user != null) {
			if (user instanceof Admin) {
				loggedIn = true;
				setDisplayLogin(true);
				return "/admin/home?faces-redirect=true";
			} 
			if (user instanceof Customer) {
				loggedIn = true;
				setDisplayLogin(true);
				return "/customer/home?faces-redirect=true";
			}
		} else {
			setDisplayLogin(false);
			FacesMessage message = new FacesMessage("     bad credentials ..");
			FacesContext.getCurrentInstance().addMessage(
					"login_form:login_submit", message);
		}
		return null;

	}

	public String doLogout() {
		String navigateTo=null;
		FacesContext.getCurrentInstance().getExternalContext()
		.invalidateSession();
		
		navigateTo = "/welcome?faces-redirect=true";

		return navigateTo;

	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public boolean isDisplayLogin() {
		return displayLogin;
	}

	public void setDisplayLogin(boolean displayLogin) {
		this.displayLogin = displayLogin;
	}
}

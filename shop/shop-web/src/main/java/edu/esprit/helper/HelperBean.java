package edu.esprit.helper;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import edu.esprit.shop.persistence.Category;
import edu.esprit.shop.services.CatalogServiceLocal;

@ManagedBean(name = "helperBean")
@ApplicationScoped
public class HelperBean {

	@EJB
	private CatalogServiceLocal catalogServiceLocal;

	public Category findByName(String name) {
		return catalogServiceLocal.findCategoryByName(name);
	}

}

package edu.esprit.convertes;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import edu.esprit.helper.HelperBean;
import edu.esprit.shop.persistence.Category;

@FacesConverter(value = "categoryConverter")
public class CategoryConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		Category category = null;
		if(!value.trim().equals("")){
			HelperBean helper = context.getApplication().evaluateExpressionGet(
					context, "#{helperBean}", HelperBean.class);
			category=helper.findByName(value);
		}
		return category;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		String ca = null;
		if (value == null || value.equals("")) {
			ca = "";
		} else {
			ca= ((Category) value).getName();
		}
		return ca;

	}

}

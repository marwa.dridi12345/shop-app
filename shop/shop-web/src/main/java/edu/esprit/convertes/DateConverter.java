package edu.esprit.convertes;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

@FacesConverter("myDateTimeConverter")
public class DateConverter extends DateTimeConverter {

	public DateConverter() {
		setPattern("yy/mm/dd");
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		// if(value!=null && value.length() != getPattern().length()){
		// System.out.println(getPattern().toString());
		// FacesContext.getCurrentInstance().addMessage("formadd:idate", new
		// FacesMessage(" date format error"));
		// throw new ConverterException("invalid format");
		// }
		return super.getAsObject(context, component, value);
	}

}
